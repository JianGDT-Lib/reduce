# reduce短网址平台

#### 技术说明

    本项目为Coody Framework+Minicat首秀，短网址服务平台。部署在服务器，使用短域名解析即可提供服务。

    全项目打包后(带前端页面)大小约：5.58M，非常精简。

    前端采用Amaze UI，后端采用Coody Framework MVC，数据库采用H2DB


Coody Framework地址：[https://gitee.com/coodyer/Coody-Framework](https://gitee.com/coodyer/Coody-Framework)

#### 基本使用

导入Maven项目，运行访问即可

也可以Maven Install 构建Jar包，丢服务器java -jar 运行。记得配置数据库。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0110/173922_5072b0fb_1200611.png "8.png")

#### 随便丢几张图

###### 登录页面

![输入图片说明](https://images.gitee.com/uploads/images/2020/0110/171636_d9d9093f_1200611.png "1.png")

###### 注册页面

![输入图片说明](https://images.gitee.com/uploads/images/2020/0110/171653_0950dff5_1200611.png "2.png")

###### 找回密码页面

![输入图片说明](https://images.gitee.com/uploads/images/2020/0110/171709_80999218_1200611.png "3.png")

###### 用户首页

![输入图片说明](https://images.gitee.com/uploads/images/2020/0110/171729_a20c28b9_1200611.png "4.png")

###### 应用页面

![输入图片说明](https://images.gitee.com/uploads/images/2020/0110/171743_a4f5b2c0_1200611.png "5.png")

###### 短网址页面

![输入图片说明](https://images.gitee.com/uploads/images/2020/0110/171756_649adeb7_1200611.png "6.png")


### 版权说明：

    作者：Coody
    
    版权：©2014-2020 Test404 All right reserved. 版权所有

    反馈邮箱：644556636@qq.com

